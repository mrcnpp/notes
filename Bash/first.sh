#!/bin/bash




if [ -z $1 ]
then
	echo "Usage nmap.sh IP"
elif [[ $1 =~ ^([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})$ ]]
then
	echo "OK"
	nmap -p- $1 -oA $1 >/dev/null 
else 
	echo "IP non valido"
fi
