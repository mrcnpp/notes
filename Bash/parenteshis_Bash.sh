#!/bin/bash
#Prints as {test1 test2 test3}
echo {test1 test2 test3}
#Prints one element per line
echo {test1 test2 test3}
#Prints \${test1 test2 test3}\$
echo \${test1 test2 test3}\$
#Works print $test1$ $test2$ $test3$ 
echo \${test1,test2,test3}\$

