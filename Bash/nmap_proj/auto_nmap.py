#! /usr/bin/python3

import os
import subprocess
import re
import sys
ports_1 = []
ports= ' '


def get_ports(str):
    ports=[]
    os.system('nmap -p- '+ip+' -oA '+ip+' >/dev/null')
    #with open('./'+ip+'gnmap') as file:
    with open('/home/marco/Desktop/monteverde.gnmap') as file:
        contents=file.read()
        ports =re.findall(r'\s(\d{1,5})/', contents)
        print(ports)
        for port in ports:
            port.split('/')
            ports_1.append(port)
        return ports_1

def full_scan(ip,ports):
    ports_str=""
    ports_str=','.join(ports)
    os.system('nmap -sC -sV -A -p '+ports_str+' -oA '+ip+' '+ip)
    print(ports_str)

    return None
def port_21():
    if  "21" in ports_1:
    return None


def port_23():
    if  "23" in ports_1:
    return None


def port_25():
    if  "25" in ports_1:
    return None

def port_53():
    if "53" in ports_1:
        os.system('dig ')


def port_80(ip):
    if  "80" in ports_1:
        os.system('nikto -h http://'+ip+' -o nikto_http_'+ip+'.txt')
        os.system('dirb http://'+ip+' -o dirb_http_'+ip+'.txt')
        print("function called")

def port_88():
    if  "88" in ports_1:
    return None
def port_139():
    if  "139" in ports_1:
    return None
def port_443(ip):
    if  "443" in ports_1:
        os.system('nikto -h https://'+ip+' -o nikto_https_'+ip+'.txt')
        os.system('dirb https://'+ip+' -o dirb_https_'+ip+'.txt')
    return None
def port_445():
    if  "445" in ports_1:
    return None
def port_5985():
    if  "5985" in ports_1:
    print("Suggestion Port 5985 is open maybe 'Evil-winrm' can help")
    return None
def port_8000(ip):
    if  "8000" in ports_1:
        os.system('nikto -h https://'+ip+' -o nikto_http_8000'+ip+'.txt')
        os.system('dirb https://'+ip+' -o dirb_http_8000'+ip+'.txt')
    return None
def port_8080(ip):
    if  "8080" in ports_1:
        os.system('nikto -h https://'+ip+' -o nikto_http_8080'+ip+'.txt')
        os.system('dirb https://'+ip+' -o dirb_http_8080'+ip+'.txt')
    return None
def port_8443(ip):
    if  "8443" in ports_1:
        os.system('nikto -h https://'+ip+' -o nikto_http_8443'+ip+'.txt')
        os.system('dirb https://'+ip+' -o dirb_http_8443'+ip+'.txt')
    return None
def port_8888(ip):
    if  "8888" in ports_1:
        os.system('nikto -h https://'+ip+' -o nikto_http_8888'+ip+'.txt')
        os.system('dirb https://'+ip+' -o dirb_http_8888'+ip+'.txt')
    return None


#MAIN
if not(len(sys.argv)==1) and re.match(r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}',sys.argv[-1]):
    ip=input("Inserisci indirizzo ip\n")
    if re.match(r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}',ip) is not None:
        print(ip+' OK')
        ports=get_ports(ip)
        full_scan(ip,ports)

else:
    print("Usage \"python3 ./auto_nmap.py <IP>\" OR \"./auto_nmap.py <IP>")
