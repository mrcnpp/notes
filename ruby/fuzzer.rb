#!/usr/bin/ruby

#USAGE ruby fuzzer.rb www.website.com [ext1,ext2,ext3] wordlist
require 'net/http'

class Http_Requester
  def request
    exts = ARGV[1].split(',')
    exts << ""
    if ARGV[2]==nil
      pages = File.read("users.txt").split
    else
      pages = File.read(ARGV[2].to_s)
    end
    for page in pages do
      for ext in exts do
        r=Net::HTTP.get_response(ARGV[0], '/'+page.to_s)
        if r.code != '404'
          puts('TESTED PAGE: '+ page.to_s+ ' HTTP CODE: ' + r.code + ' EXTENSION: '+ ext.to_s)
        end
      end
    end
    return nil
  end
end

test=Http_Requester.new()
puts test.request()
