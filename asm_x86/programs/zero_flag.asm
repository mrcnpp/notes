format PE console
entry start
include 'win32a.inc'
;=====================;
section '.text' code readable executable

not_zero:
	mov eax,1
	call print_eax
	jmp fine
zero:	
	mov eax,0
	call print_eax
	jmp fine
start:	
	mov edx,4
	mov ecx,4
	sub edx,ecx
	jz  zero 
	
fine:	
	push 0
	call [ExitProcess]

include 'training.inc'
