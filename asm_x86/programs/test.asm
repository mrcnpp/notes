format PE console
entry start

include 'win32a.inc'
;====================;
section '.text' code readable executable

start:	
	xor eax,eax
	xor ecx,ecx
	add ecx,4
	add eax,ecx
	mov eax,1
	call print_eax
	call read_hex
	
	push 0
	call [ExitProcess]  

include 'training.inc'
