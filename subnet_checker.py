from netaddr import *
import sys

ip_list_=[]
subnet_list_=[]
ip_list=open('result.txt','r+')
subnet_list=open('subnet.txt','r+')
file=open('ordered.txt','w+')
ip_list_=ip_list.readlines()
subnet_list_=subnet_list.readlines()

for i in range(0,len(subnet_list_)):
    file.write(subnet_list_[i]+'\n')
    global counter
    counter=0
    for k in range(0,len(ip_list_)) :
        if ip_list_[k] in IPNetwork(subnet_list_[i]):
            file.write(ip_list_[k])
            counter+=1
    file.write('#IP number: '+str(counter)+'\n')
    file.write('========================================\n')

file.close()
subnet_list.close()
ip_list.close()
